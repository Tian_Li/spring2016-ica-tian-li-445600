<?php 

if(empty($_POST["name"]) && $_POST["name"]!="0"){
	echo "missing name";
	exit;
}
if(empty($_POST["species"]) && $_POST["species"]!="0"){
	echo "missing species";
	exit;
}
if(empty($_POST["weight"]) && $_POST["weight"]!="0"){
	echo "missing weight";
	exit;
}
if(empty($_POST["description"]) && $_POST["description"]!="0"){
	echo "missing description";
	exit;
}
if(isset($_POST["picture"])){
	echo "missing picture";
	exit;
}

$name=htmlentities($_POST["name"]);
$species=htmlentities($_POST["species"]);
$weight=(float)htmlentities($_POST["weight"]);
$description=htmlentities($_POST["description"]);

$filename=$_FILES["picture"]["name"];
$filetype=$_FILES["picture"]["type"];
$tmpfname=$_FILES["file"]["tmp_name"];

$picture=htmlentities($filename);

echo "Picture: ". $picture;

//connect the database
$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'petdb');
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}



$stmt=$mysqli->prepare("INSERT INTO pets (name, species, weight, description, filename) VALUES (?, ?, ?, ?, ?)");
if(!$stmt) {
	printf("Query prep failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param(’ssfss’,$name, $species, $weight, $description, $picture);

if($stmt->execute()) {
	printf("Execute failed: %s\n", $mysqli->error);
	exit;
}
$stmt->close();
//move the file
$dir="/home/tian/public_html/";
if (file_exists($dir . $picture)) {
	echo $filename . " already exists.";
}
else{
	move_uploaded_file($tmpfname, $dir . $picture);
	echo "Now stored in: " . $dir . $picture;
	header("Location: pet-listings.php");
}




?>